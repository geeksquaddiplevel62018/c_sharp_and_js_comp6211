using System;

namespace comp6211_18a
{
    class Employee
    {
        // Write a class Employee to have attributes: ID (int), name (string) and address (string).
        // Create two constructors (with and without parameter values).
        // Also, create getter and setter methods for each attributes.
        // Once this class is defined. Create two objects of this class in the main method. 
        
        public int id { get; set; }
        public string name { get; set; }
        public string addr { get; set; }

        public Employee()
        {
            this.id = 0;
            this.name = "No Name";
            this.addr = "No Address";

            printInfo();
        }

        public Employee(int id, string name, string addr)
        {
            this.id = id;
            this.name = name;
            this.addr = addr;

            printInfo();
        }

        private void printInfo()
        {
            Console.WriteLine("-----");
            Console.WriteLine("Print Employee's Info");
            Console.WriteLine("-----");
            Console.WriteLine("ID : " + this.id);
            Console.WriteLine("Name : " + this.name);
            Console.WriteLine("Address : " + this.addr);
            Console.WriteLine("----------------------------");
            Console.WriteLine();
        }
    }
}