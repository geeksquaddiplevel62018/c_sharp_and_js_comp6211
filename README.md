# COMP6211 1st C Sharp and JS

---------------------------------------------------
This is about 'Simple Programming inclass Quiz'
The Quiz is following.
---------------------------------------------------

Program.cs
------------------

Basic Exercises
Write a program to print 'Hello' on screen and then print your name on a separate line.

Expected Output :
Hello 
Alexandra Abramov

Write a program to print the sum of two numbers. 

Test Data: 
74 + 36 
Expected Output :
110

Write a program to divide two numbers and print on the screen.
Test Data : 
50/3
Expected Output :
16

Write a program that takes two numbers as input and display the product of two numbers.
Test Data:
Input first number: 25
Input second number: 5
Expected Output :
25 x 5 = 125

Write a program that takes a number as input and prints its multiplication table upto 10 (hint: use for or while loop). 
Test Data:
Input a number: 8
Expected Output :
8 x 1 = 8
8 x 2 = 16
8 x 3 = 24
...
8 x 10 = 80

Write a program to print the area and perimeter of a circle. 
Test Data:
Radius = 7.5 
Expected Output
Perimeter is = 47.12388980384689 
Area is = 176.71458676442586

Write a program to swap two variables

Conditional Statement Exercises

Write a program to get a number from the user and print whether it is positive or negative.

Test Data
Input number: 35
Expected Output :
Number is positive

Take three numbers from the user and print the greatest number. 

Test Data
Input the 1st number: 25 
Input the 2nd number: 78 
Input the 3rd number: 87
Expected Output :
The greatest: 87



javascript.html
------------------

Write a one dimensioanl array of size 5 and populate that with random numbers.

Write a Java program to sort a numeric array and a string array

Write a Java program to remove a specific element from an array.

Write a Java program to find the maximum and minimum value of an array.

Write a Java program to reverse an array of integer values.


Program.cs, Employee.cs
------------------------
Write a class Employee to have attributes: ID (int), name (string) and address (string). Create two constructors (with and without parameter values). Also, create getter and setter methods for each attributes. Once this class is defined. Create two objects of this class in the main method. 

Program.cs
------------------
Write a Program that uses stacks to reverse a user inputted string
