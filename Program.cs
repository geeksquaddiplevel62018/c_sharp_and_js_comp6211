﻿using System;

namespace comp6211_18a
{
    class Program
    {
        static void Main(string[] args)
        {
            int maxCnt = 5, i=0;

            // Basic Exercises
            // 1. Write a program to print 'Hello' on screen and then print your name on a separate line.
            // Expected Output :
            // Hello 
            // Alexandra Abramov
            hello();

            // 2. Write a program to print the sum of two numbers. 
            // Test Data: 
            // 74 + 36 
            // Expected Output :
            // 110
            sum();

            // 3. Write a program to divide two numbers and print on the screen.
            // Test Data : 
            // 50/3
            // Expected Output :
            // 16
            divide();

            // 4. Write a program that takes two numbers as input and display the product of two numbers.
            // Test Data:
            // Input first number: 25
            // Input second number: 5
            // Expected Output :
            // 25 x 5 = 125
            i=0;
            while(!multiplyWithNumbers() && i++ < maxCnt);

            // Write a program that takes a number as input and prints its multiplication table upto 10 (hint: use for or while loop). 
            // Test Data:
            // Input a number: 8
            // Expected Output :
            // 8 x 1 = 8
            // 8 x 2 = 16
            // 8 x 3 = 24
            // ...
            // 8 x 10 = 80
            i=0;
            while(!mutiplicationTable() && i++ < maxCnt);

            // Write a program to print the area and perimeter of a circle. 
            // Test Data:
            // Radius = 7.5 
            // Expected Output
            // Perimeter is = 47.12388980384689 
            // Area is = 176.71458676442586
            areaNPerimeter();

            // Write a program to swap two variables
            i=0;
            while(!swap() && i++ < maxCnt);

            // Conditional Statement Exercises
            // Write a program to get a number from the user and print whether it is positive or negative.
            // Test Data
            // Input number: 35
            // Expected Output :
            // Number is positive
            i=0;
            while(!posNneg() && i++ < maxCnt);

            // Take three numbers from the user and print the greatest number. 
            // Test Data
            // Input the 1st number: 25 
            // Input the 2nd number: 78 
            // Input the 3rd number: 87
            // Expected Output :
            // The greatest: 87
            i=0;
            while(!maxNum() && i++ < maxCnt);


            // Write a class Employee to have attributes: ID (int), name (string) and address (string).
            // Create two constructors (with and without parameter values).
            // Also, create getter and setter methods for each attributes.
            // Once this class is defined. Create two objects of this class in the main method. 
            Employee employeeWithoutInfo = new Employee();
            Employee employeeWithInfo = new Employee(100, "Sing", "145 Good Street, Welcome Bay, Tauranga, New Zealand");

            // Write a Program that uses stack to reverse a user inputted string
            stack();
        }

        // Basic Exercises
        // 1. Write a program to print 'Hello' on screen and then print your name on a separate line.
        // Expected Output :
        // Hello 
        // Alexandra Abramov
        static void hello()
        {
            Console.WriteLine("Write a program to print 'Hello' on screen and then print your name on a separate line.");
            Console.WriteLine("Expected Output:");
            Console.WriteLine("Hello");
            Console.WriteLine("Shinkee Cho (Sing)");
            Console.WriteLine("----------------------------");
            Console.WriteLine();
        }

        // 2. Write a program to print the sum of two numbers. 
        // Test Data: 
        // 74 + 36 
        // Expected Output :
        // 110
        static void sum()
        {
            Console.WriteLine("Write a program to print the sum of two numbers.");
            Console.WriteLine("Test Data:");
            Console.WriteLine("74 + 36");
            Console.WriteLine("Expected Output:");
            Console.WriteLine(74+36);
            Console.WriteLine("----------------------------");
            Console.WriteLine();
        }

        // 3. Write a program to divide two numbers and print on the screen.
        // Test Data : 
        // 50/3
        // Expected Output :
        // 16
        static void divide()
        {
            Console.WriteLine("Write a program to divide two numbers and print on the screen.");
            Console.WriteLine("Test Data:");
            Console.WriteLine("50/3");
            Console.WriteLine("Expected Output:");
            Console.WriteLine(50/3);
            Console.WriteLine("----------------------------");
            Console.WriteLine();
        }


        // 4. Write a program that takes two numbers as input and display the product of two numbers.
        // Test Data:
        // Input first number: 25
        // Input second number: 5
        // Expected Output :
        // 25 x 5 = 125
        static bool multiplyWithNumbers()
        {
            string strNum;
            int num1, num2;

            Console.WriteLine("Write a program that takes two numbers as input and display the product of two numbers.");
            
            Console.WriteLine("Test Data:");

            Console.Write("Input first number: ");
            strNum = Console.ReadLine();
            if (int.TryParse(strNum, out num1) == false)
            {
                Console.WriteLine("You typed in wrong type of number.");
                return false;
            }

            Console.Write("Input second number: ");
            strNum = Console.ReadLine();
            if (int.TryParse(strNum, out num2) == false)
            {
                Console.WriteLine("You typed in wrong type of number.");
                return false;
            }

            Console.WriteLine("Expected Output:");
            Console.WriteLine(num1 + " x " + num2 + " = " + num1*num2);
            Console.WriteLine("----------------------------");
            Console.WriteLine();

            return true;
        }

        // 5. Write a program that takes a number as input and prints its multiplication table upto 10 (hint: use for or while loop). 
        // Test Data:
        // Input a number: 8
        // Expected Output :
        // 8 x 1 = 8
        // 8 x 2 = 16
        // 8 x 3 = 24
        // ...
        // 8 x 10 = 80
        static bool mutiplicationTable()
        {
            string strNum;
            int num, i = 0, maxCnt = 10;

            Console.WriteLine("Write a program that takes a number as input and prints its multiplication table upto 10 (hint: use for or while loop).");
            Console.WriteLine("Test Data:");

            Console.Write("Input first number: ");
            strNum = Console.ReadLine();
            if (int.TryParse(strNum, out num) == false)
            {
                Console.WriteLine("You typed in wrong type of number.");
                return false;
            }

            Console.WriteLine("Expected Output:");
            for(i=1;i<=maxCnt;i++)
            {
                Console.WriteLine(strNum + " x " + i + " = " + num*i);
            }
            
            Console.WriteLine("----------------------------");
            Console.WriteLine();
            
            return true;
        }

        // Write a program to print the area and perimeter of a circle. 
        // Test Data:
        // Radius = 7.5 
        // Expected Output
        // Perimeter is = 47.12388980384689 
        // Area is = 176.71458676442586
        static void areaNPerimeter()
        {
            Console.WriteLine("Write a program to print the area and perimeter of a circle.");
            Console.WriteLine("Test Data:");
            Console.WriteLine("Radius = 7.5");
            Console.WriteLine("Expected Output");
            Console.WriteLine("Perimeter is = " + 2 * 7.5 * Math.PI);
            Console.WriteLine("Area is = " + 2 * Math.Pow(7.5, 2) * Math.PI);
            Console.WriteLine("----------------------------");
            Console.WriteLine();
        }

        // Write a program to swap two variables
        static bool swap()
        {
            string strNum1, strNum2;
            int num1, num2;
            int tempNum;

            Console.WriteLine("Write a program to swap two variables.");
            Console.WriteLine("Test Data:");

            Console.Write("Input first number: ");
            strNum1 = Console.ReadLine();
            if (int.TryParse(strNum1, out num1) == false)
            {
                Console.WriteLine("You typed in wrong type of number.");
                return false;
            }

            Console.Write("Input second number: ");
            strNum2 = Console.ReadLine();
            if (int.TryParse(strNum2, out num2) == false)
            {
                Console.WriteLine("You typed in wrong type of number.");
                return false;
            }

            // swap numbers here
            tempNum = num1;
            num1 = num2;
            num2 = tempNum;

            Console.WriteLine("Expected Output:");
            Console.WriteLine("First number is : " + num1);
            Console.WriteLine("Second number is : " + num2);
            
            Console.WriteLine("----------------------------");
            Console.WriteLine();

            return true;
        }

        // Conditional Statement Exercises
        // Write a program to get a number from the user and print whether it is positive or negative.
        // Test Data
        // Input number: 35
        // Expected Output :
        // Number is positive
        static bool posNneg()
        {
            string strNum;
            int num;
            string result;

            Console.WriteLine("Write a program to get a number from the user and print whether it is positive or negative.");
            Console.WriteLine("Test Data:");

            Console.Write("Input number: ");
            strNum = Console.ReadLine();
            if (int.TryParse(strNum, out num) == false)
            {
                Console.WriteLine("You typed in wrong type of number.");
                return false;
            }
            
            // 
            if(num >= 0){ result = "positive"; }
            else { result = "negative"; }

            Console.WriteLine("Expected Output:");
            Console.WriteLine("Number is : " + result);
            Console.WriteLine("----------------------------");
            Console.WriteLine();

            return true;
        }

        // Take three numbers from the user and print the greatest number. 
        // Test Data
        // Input the 1st number: 25 
        // Input the 2nd number: 78 
        // Input the 3rd number: 87
        // Expected Output :
        // The greatest: 87
        static bool maxNum()
        {
            string strNum;
            int[] numbers;
            int maxNum, i;

            numbers = new int[3];

            Console.WriteLine("Take three numbers from the user and print the greatest number.");
            Console.WriteLine("Test Data:");

            Console.Write("Input the 1st number: ");
            strNum = Console.ReadLine();
            if (int.TryParse(strNum, out numbers[0]) == false)
            {
                Console.WriteLine("You typed in wrong type of number.");
                return false;
            }

            Console.Write("Input the 2nd number: ");
            strNum = Console.ReadLine();
            if (int.TryParse(strNum, out numbers[1]) == false)
            {
                Console.WriteLine("You typed in wrong type of number.");
                return false;
            }

            Console.Write("Input the 3rd number: ");
            strNum = Console.ReadLine();
            if (int.TryParse(strNum, out numbers[2]) == false)
            {
                Console.WriteLine("You typed in wrong type of number.");
                return false;
            }

            // Find out the largest number.
            maxNum = numbers[0];
            for(i=0;i<3;i++)
            {
                if(maxNum < numbers[i])
                {
                    maxNum = numbers[i];
                }
            }
            
            Console.WriteLine("Expected Output:");
            Console.WriteLine("Number is : " + maxNum);
            Console.WriteLine("----------------------------");
            Console.WriteLine();
            return true;
        }

        // Write a Program that uses stack to reverse a user inputted string
        static void stack()
        {
            string strStack;
            int i, cntChar;

            // Push string into stack
            Console.Write("Type in a string : ");
            strStack = Console.ReadLine();
            cntChar = strStack.Length;

            Console.WriteLine("----------------------------");
            Console.Write("Result : ");

            // Reverse string
            for(i=cntChar-1; i>=0; i--)
            {
                Console.Write(strStack[i]);
            }

            Console.WriteLine();
            Console.ReadLine();
        }
    }
}
